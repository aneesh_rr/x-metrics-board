/*===============================================================
    creating xmetrics_charts for storing chart name
===============================================================*/
CREATE TABLE `xmetrics_charts` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`chart_name` VARCHAR(200) NOT NULL COLLATE 'utf8mb4_0900_ai_ci',
	`is_deleted` TINYINT(3) NOT NULL DEFAULT '0',
	`created_by` INT(10) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_by` INT(10) NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_0900_ai_ci'
ENGINE=MyISAM
AUTO_INCREMENT=4
;

/*===============================================================
    creating xmetrics_charts_section for storing sections
===============================================================*/

CREATE TABLE `xmetrics_charts_section` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`chart_id` INT(10) NOT NULL,
	`name` VARCHAR(200) NOT NULL COLLATE 'utf8mb4_0900_ai_ci',
	`section_type` ENUM('top','bottom','left','right','user') NOT NULL DEFAULT 'top' COMMENT '1 = TOP, 2 = BOTTOM, 3 = LEFT, 4 = RIGHT, 5 = USER' COLLATE 'utf8mb4_0900_ai_ci',
	`is_deleted` TINYINT(3) NOT NULL DEFAULT '0',
	`created_by` INT(10) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_by` INT(10) NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `FK_x_metrics_charts_section_x_metrics_charts` (`chart_id`) USING BTREE
)
COLLATE='utf8mb4_0900_ai_ci'
ENGINE=MyISAM
AUTO_INCREMENT=29
;


/*===============================================================
    creating xmetrics_charts_kpi for storing KPI datas
===============================================================*/

CREATE TABLE `xmetrics_charts_kpi` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(200) NOT NULL COLLATE 'utf8mb4_0900_ai_ci',
	`chart_id` INT(10) NOT NULL,
	`section_id` INT(10) NOT NULL,
	`kpi_id` INT(10) NOT NULL,
	`kpi_type` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8mb4_0900_ai_ci',
	`is_deleted` TINYINT(3) NOT NULL DEFAULT '0',
	`sort_order` INT(10) NOT NULL,
	`created_by` INT(10) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_by` INT(10) NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `FK_x_metrics_charts_kpi_x_metrics_charts_section` (`section_id`) USING BTREE
)
COLLATE='utf8mb4_0900_ai_ci'
ENGINE=MyISAM
AUTO_INCREMENT=35
;

/*===============================================================
    creating xmetrics_charts_kpi for storing KPI relations
===============================================================*/

CREATE TABLE `xmetrics_chart_relations` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`chart_id` INT(10) NOT NULL,
	`row_chart_kpi` INT(10) NOT NULL,
	`column_chart_kpi` INT(10) NOT NULL,
	`type` ENUM('casual_relationship','primary_global','primary_regional','secondary_global','secondary_regional') NOT NULL DEFAULT 'casual_relationship' COMMENT '1 =\'Casual Relationship\', 2 = \'Primary Responsibility- Global\', 3 = \'Primary Responsibility- Regional\', 4 = \'Secondary Responsibility- Global\', 5= \'Secondary Responsibility- Regional\'' COLLATE 'utf8mb4_0900_ai_ci',
	`created_by` INT(10) NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `FK_x_metrics_chart_relations_x_metrics_charts` (`chart_id`) USING BTREE
)
COLLATE='utf8mb4_0900_ai_ci'
ENGINE=MyISAM
AUTO_INCREMENT=7
;

