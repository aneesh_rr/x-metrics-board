$(function () {

    // const tableMaps = {
    //     'toggle9': 'content9',
    //     'toggle': 'content',
    //     'abc': 'content1',
    //     'toggle2': 'content2',
    //     'toggle3': 'content3',
    //     'toggle4': 'content4',
    //     'toggle5': 'content5',
    //     'toggle6': 'content6',
    //     'toggle7': 'content7',
    //     'toggle8': 'content8',
    // }

    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            $(this).trigger('collapse')
            children.find(' > span').trigger('collapse')  //collapse all child
        } else {
            $(this).trigger('expand')
        }
        e.stopPropagation();
    });

    // Collapsing an item
    $('.tree li.parent_li > span').on('collapse', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        children.hide('fast');
        $(this).attr('title', 'Expand this branch').find(' > i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
        $(this).data('open', false)
        e.stopPropagation();

        if ($(this).hasClass("toggle9") ) {
            $(".content9").removeClass('active');
        }
        if ($(this).hasClass("toggle") )  {
            $(".content").removeClass('active');
        }
        if ($(this).hasClass("abc") )  {
            $(".content1").removeClass('active');
        }
        if ($(this).hasClass("toggle2") )  {
            $(".content2").removeClass('active');
        }
        if ($(this).hasClass("toggle3") )  {
            $(".content3").removeClass('active');
        }
        if ($(this).hasClass("toggle4") )  {
            $(".content4").removeClass('active');
        }
        if ($(this).hasClass("toggle5") )  {
            $(".content5").removeClass('active');
        }
        if ($(this).hasClass("toggle6") )  {
            $(".content6").removeClass('active');
        }
        if ($(this).hasClass("toggle7") )  {
            $(".content7").removeClass('active');
        }
        if ($(this).hasClass("toggle8") )  {
            $(".content8").removeClass('active');
        }

    })

    // Expanding an item
    $('.tree li.parent_li > span').on('expand', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        children.show('fast');
        $(this).attr('title', 'Collapse this branch').find(' > i').addClass('fa-chevron-down').removeClass('fa-chevron-right');
        $(this).data('open', true)
        e.stopPropagation();
        
        if ($(this).hasClass("toggle9") ) {

            $(".content9").addClass('active');
        }
        if ($(this).hasClass("toggle") )  {
            $(".content9").removeClass('active');
            $(".content").addClass('active');
        }
        if ($(this).hasClass("abc") )  {
             $(".content2").removeClass('active');
            $(".content1").addClass('active');
        }
        if ($(this).hasClass("toggle2") )  {
             // $(".content").removeClass('active');
            $(".content2").addClass('active');
        }
        if ($(this).hasClass("toggle3") )  {

            $(".content3").addClass('active');
        }
        if ($(this).hasClass("toggle4") )  {
            $(".content4").addClass('active');
        }
        if ($(this).hasClass("toggle5") )  {
            $(".content5").addClass('active');
        }
        if ($(this).hasClass("toggle6") )  {
             $(".content5").removeClass('active');
            $(".content6").addClass('active');
        }
        if ($(this).hasClass("toggle7") )  {
            $(".content7").addClass('active');
        }
        if ($(this).hasClass("toggle8") )  {
            $(".content8").addClass('active');
        }
    })


});
