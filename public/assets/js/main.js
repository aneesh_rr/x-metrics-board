
var LtsMain = {

    post: function (url, postData, hasAlert, succesCallback, errorCallback) {

        $.ajax({
            url: url,
            type: "post",
            data: postData,
            success: function (response) {
                if (hasAlert) {
                    if (response.msg)
                    {
                        if (response.status === 'error')
                        {
                            LtsMain.alert(response.msg, response.status);
                        } else {
                            LtsMain.alert(response.msg);
                        }
                    }
                }
                if (succesCallback) {
                    succesCallback(response);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                if (errorCallback) {
                    errorCallback();
                }
            }
        });

    },

    renderHtml: function (templateId, data) {
        var compiled = _.template($('#' + templateId).html());
        return compiled(data)
    },

    clearForm: function (param) {
        $(':input', param)
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
    },

    formSerialize: function (param) {
        var form_data = $(param).serializeArray();
        return form_data;
    },

    popup: function (action, html) {
        if (action === 'show') {
            $('#ltsModalPopUp').html(html);
            $('#ltsModalPopUp').modal('show');
        }
        if (action === 'hide') {
            $('#ltsModalPopUp').modal('hide');
        }
    },

    /**
     * Toastr alert
     * https://codeseven.github.io/toastr/demo.html
     * @param {type} msg
     * @param {type} status
     * @returns {undefined}
     */
    alert: function (msg, status,options) {
        var defaultOptions= {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.options = $.extend(true, {}, defaultOptions, options);

        if (status) {
            toastr[status](msg);
        } else {
            toastr["success"](msg);
        }
    },

    confirm: function (msg, callback) {
        bootbox.confirm({
            title: null,
            message: msg,
            buttons: {
                cancel: {
                    label: 'Cancel'
                },
                confirm: {
                    label: 'Confirm'
                }
            },
            callback: function (result) {
                callback(result);
            }
        });
    }, 
    
    customValidate: function(formId) {
        $('label.error').remove();
        var error = 0;
        formId = formId ? '#'+formId : 'form';
        var msg = 'This filed is required';
        $(formId).find('input,select,textarea').each(function () {
            if ($(this).attr('required')) {
                var input = $(this);
                if (input.val().trim() === '') {
                    error += 1;
                    msg = input.attr('data-validate-msg') ? input.attr('data-validate-msg') : msg;
                    $('<label class="error">'+msg+'</label>').insertAfter(input);
                }
            }
        });
        if(error === 0) {
            return true;
        }
        return false;
    },
    
    formGetData: function(param) {
        var form_input = $(param).find('input,select,textarea');
        const obj = {};
        $.each(form_input, function() {
            if ($(this).attr('data-key')!='') {
            _.set(obj, $(this).data('key') , $(this).val())
        }
        });
        return obj;
    }

};





