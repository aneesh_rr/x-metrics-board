var LtsMatrix = /** @class */ (function () {
    function LtsMatrix(options) {
        this.defaultSettings = {
            data: '',
            chart_url: 'http://127.0.0.1:8000/x-metrix/_id'
        };
        this.settings = {};
        this.settings = $.extend(true, {}, this.defaultSettings, options);
    }
    ;
    LtsMatrix.prototype.init = function () {
        this.handleEvents();
        this.buildXmatrix();
    };
    LtsMatrix.prototype.handleEvents = function () {
        var _this = this;
        $(document).on('click', '.add-kpi-btn', function () {
            var btn_id = this.id;
            var select_id = '#' + 'select_' + btn_id;
            var select_type = $(select_id).data("type");
            var i = $(select_id).data("section_index");
            var selected_kpi_id = $(select_id).val();
            var selected_kpi_value = $(select_id + " option:selected").text();
            if (selected_kpi_id != '') {
                var html = LtsMain.renderHtml('templateMatrixRows', {data: {id: '', label: selected_kpi_value, kpi_id: selected_kpi_id}, i: i});
                $('#tabul_' + select_type).append(html);
            }
            _this.checkKpiExist(select_type);
        });
        $(document).on('click', '#save', function () {
            $data = LtsMain.formGetData('#xmatrixForm');
            LtsMain.post('/add_chart_details_ajax', JSON.stringify($data['formdata']), '', function (response) {
                if (response.status === 'success') {
                    _this.reBuildContent(_this.settings.matrix_id);
                }
            }, '');
        });

        $(document).on('click', '.t-cell', function () {
            var row = $(this).data('row');
            var column = $(this).data('column');
            var details = {
                row: row,
                column: column,
                chart_id: _this.settings.matrix_id
            }
            LtsMain.post('/add-relation', JSON.stringify(details), '', function (response) {
                if (response) {
                    _this.reBuildContent(_this.settings.matrix_id);
                }
            }, '');
        });

        $(document).on('click', '.remove-x-li', function () {
            var button_id = this.id;
            var data_id = $('#' + button_id).data("id");
            var refresh_id = '#' + 'refresh_li_' + data_id;
            $(refresh_id).removeClass("hidden");
            $('#' + button_id).addClass("hidden");
            $('#' + data_id + '_is_delete').val('1');
            $(this).parent().css("background-color", "#ff00002b");
        });

        $(document).on('click', '.refresh-x-li', function () {
            var button_id = this.id;
            var data_id = $('#' + button_id).data("id");
            var remove_id = '#' + 'remove_li_' + data_id;
            $(remove_id).removeClass("hidden");
            $('#' + button_id).addClass("hidden");
            $('#' + data_id + '_is_delete').val('0');
            $(this).parent().css("background-color", "#fbfbfb");
        });

        $(document).on('click', '#new_chart_modal', function () {
            var html = LtsMain.renderHtml('templateNewChartModal');
            $('#add-new-chart').html(html);
        });

        $(document).on('click', '#save_new_chart', function () {
            var name=$('#chart_name').val();
            if (name.length < 1) {
                alert('please enter valid chart name');
            } else {
                var details = {chart_name: name};
                LtsMain.post('/add-new-chart', JSON.stringify(details), '', function (response) {
                    if (response.id) {
                        window.location.href = _this.settings.chart_url.replace('_id', response.id);
                    }
                }, '');
            }
        });
    };

    LtsMatrix.prototype.checkKpiExist = function (type) {
        $('#select_kpi_' + type).find('option').prop('disabled', false);
        $('#tabul_' + type + ' li').each(function (i) {
            var kpiId = $(this).data('kpi_id');
            $('#select_kpi_' + type).find('option[value = "' + kpiId + '"]').prop('disabled', true);
        });
        $('#select_kpi_' + type).val('');
    };

    LtsMatrix.prototype.buildXmatrix = function () {
        var data = this.settings.data;
        if ((_.size(data) > 0) && data['verify']['is_valid'] == 'valid') {
            var rows = [];
            var columns = [];
            var completeColumnData = [];
            var completeRowData = [];
            var topReverseData = _.sortBy(data['top']['data'], '').reverse();
            var leftRevers = _.sortBy(data['left']['data'], '').reverse();
            var topDataCount = _.size(data['top']['data']);
            var leftDataCount = _.size(data['left']['data']);
            var bottomDataCount = _.size(data['bottom']['data']);
            var rishtsideDataCount = _.size(data['right']['data']);
            var rishtsideDataTotalCount = _.size(data['right']['data']) + _.size(data['user']['data']);
            $.each(topReverseData, function (i, res) {
                rows.push(res);
                completeRowData.push(res);
            });
            rows.push([]);
            completeRowData.push([]);
            $.each(data['bottom']['data'], function (i, res) {
                rows.push(res);
                completeRowData.push(res);
            });
            $.each(leftRevers, function (i, res) {
                columns.push(res);
                completeColumnData.push(res);
            });
            columns.push([]);
            completeColumnData.push([]);
            $.each(data['right']['data'], function (i, res) {
                columns.push(res);
                completeColumnData.push(res);
            });
            $.each(data['user']['data'], function (i, res) {
                columns.push(res);
                completeColumnData.push(res);
            });
        }
        if ((_.size(data) > 0)) {
            var sampleData = this.sampleData();
            var html = LtsMain.renderHtml('templateMatrix', {'data': data, 'columns': columns, 'rows': rows, 'rishtsideDataTotalCount': rishtsideDataTotalCount, 'bottomDataCount': bottomDataCount, 'leftDataCount': leftDataCount, 'topDataCount': topDataCount, 'rishtsideDataCount': rishtsideDataCount, 'completeRowData': completeRowData, 'completeColumnData': completeColumnData});
            $('#xMatrixTable tbody').html(html);
            var html = LtsMain.renderHtml('templateMatrixSections', {data: data, 'sampleDataArray': sampleData});
            $('#add-x-matrix-data').html(html);
            this.checkKpiExist('top');
            this.checkKpiExist('left');
            this.checkKpiExist('right');
            this.checkKpiExist('bottom');
            this.checkKpiExist('user');
        }
    }

    LtsMatrix.prototype.reBuildContent = function (id) {
        _this = this;
        var details = {
            id: id
        }
        LtsMain.post('/x-metrix-get-content-ajax', JSON.stringify(details), '', function (response) {
            if (response) {
                _this.set('data', response.data);
                _this.buildXmatrix();
                $('#add-x-matrix-data').modal('hide');
                alert('x-matrix updated', 'success', '');
            }
        }, '');
    };

    LtsMatrix.prototype.set = function (key, val) {
        this.settings[key] = val;
        return this;
    };

    LtsMatrix.prototype.sampleData = function () {
        var sampleData = [
            {id: '10', label: 'Professional Development: Achieve targeted professional certification in Supply Chain, Quality, and Project Management disciplines'},
            {id: '11', label: 'Process Discipline: Improve inventory accuracu & transactional discipline within Symix and SAP business systems'},
            {id: '12', label: 'CONNEX: Implement SAP in US and Mexico by YE08. Achieve critical preparation milestones for Europe and Asia'},
            {id: '13', label: 'Southern China: Complete preparations for consolidation of manufacturing facilities. Focus on Shi Yan first'},
            {id: '14', label: 'Rebalance Global Operations Footprint: Achieve 2008 Project Milestones in BW, HF, FY, Mex, and E.Eur. Start up operations in India'},
            {id: '15', label: 'Provide associates with the opportunity to develop and grow'},
            {id: '16', label: 'Establish process discipline as a competitive Advantage within our culture'},
            {id: '17', label: 'Improve operating margins and working capital turns'},
            {id: '18', label: 'Demonstrate DFSS Success on 6 New Product Designs in 2008. Apply DFSS to 2 projects per trained engineer per year'},
            {id: '19', label: "Include certification requirement in PDR's of all supply chain, quality, and project leadership positions. Track compliance via PDR process"},
            {id: '20', label: 'Implement a World Class Lean Enabling ERP System in Prioritized Phases'},
            {id: '21', label: 'Accelerate the implementation of Global Integrated Supply Chain & Engineering Strategy'},
            {id: '22', label: 'Achieve Lean Enterprise Driven Culture in All Functions'},
            {id: '23', label: 'Complete produc and technology moves from BW and HR'},
            {id: '24', label: 'Ramp up production in Mexico, India and Fu Yong'},
            {id: '25', label: 'Capitalize on Emerging Market Opportunities & Strategic Acquisitions. resulting in 35% of total revenue growth from 2007 to 2011.'},
            {id: '26', label: "Achieve world class integrated supply chain Metrices( Zero Incidents, 5 Sigma Level, >95% OTD to Customer's Dock,>$146k/EE,>12 Turns.)"},
            {id: '27', label: 'Double Revenue from $350M to $700M by 2011. Increase Market Share from 10% of $3.2B market in 2006 to 16% of $4.3B market in 2011'},
        ]
        return sampleData;
    };
    return LtsMatrix;
}());

