<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XMetricsChartRelations
 * @ORM\Entity(repositoryClass="App\Repository\XMetricsChartRelationsRepository")
 * 
 */
class XMetricsChartRelations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="chart_id", type="integer", nullable=false)
     */
    private $chartId;

    /**
     * @var int
     *
     * @ORM\Column(name="row_chart_kpi", type="integer", nullable=false)
     */
    private $rowChartKpi;

    /**
     * @var int
     *
     * @ORM\Column(name="column_chart_kpi", type="integer", nullable=false)
     */
    private $columnChartKpi;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=0, nullable=false, options={"default"="casual_relationship","comment"="1 ='Casual Relationship', 2 = 'Primary Responsibility- Global', 3 = 'Primary Responsibility- Regional', 4 = 'Secondary Responsibility- Global', 5= 'Secondary Responsibility- Regional'"})
     */
    private $type = 'casual_relationship';

    /**
     * @var int
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChartId(): ?int
    {
        return $this->chartId;
    }

    public function setChartId(int $chartId): self
    {
        $this->chartId = $chartId;

        return $this;
    }

    public function getRowChartKpi(): ?int
    {
        return $this->rowChartKpi;
    }

    public function setRowChartKpi(int $rowChartKpi): self
    {
        $this->rowChartKpi = $rowChartKpi;

        return $this;
    }

    public function getColumnChartKpi(): ?int
    {
        return $this->columnChartKpi;
    }

    public function setColumnChartKpi(int $columnChartKpi): self
    {
        $this->columnChartKpi = $columnChartKpi;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }


}
