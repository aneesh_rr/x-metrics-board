<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\XMetricsChartsRepository;
use App\Repository\XMetricsChartsSectionRepository;
use App\Repository\XMetricsChartsKpiRepository;
use App\Repository\XMetricsChartRelationsRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MetrixController extends AbstractController {

    private $xMetricsChartsRepository;
    private $xMetricsChartsSectionRepository;
    private $xMetricsChartsKpiRepository;
    private $xMetricsChartRelationsRepository;

    public function __construct(XMetricsChartsRepository $xMetricsChartsRepository, XMetricsChartsSectionRepository $xMetricsChartsSectionRepository, XMetricsChartsKpiRepository $xMetricsChartsKpiRepository, XMetricsChartRelationsRepository $xMetricsChartRelationsRepository) {
        $this->xMetricsChartsRepository = $xMetricsChartsRepository;
        $this->xMetricsChartsSectionRepository = $xMetricsChartsSectionRepository;
        $this->xMetricsChartsKpiRepository = $xMetricsChartsKpiRepository;
        $this->xMetricsChartRelationsRepository = $xMetricsChartRelationsRepository;
    }

    public function index($id = ''): Response {
        $data = $this->getChartDetails($id);
        if (count($data)) {
            return $this->render('table/x-metrix.html.twig', [
                        'controller_name' => 'TableController',
                        'data' => $data,
                        'matrixID' => $id
            ]);
        } else {
            throw new AccessDeniedHttpException('No Chart found');
        }
    }

    public function addChartAction(Request $request) {
        $details = json_decode($request->getContent(), true);
        $response_text = '';
        if (!isset($details['chart_id'])) {
            $response_text = 'Quad chart added succesfully';
        } else {
            $response_text = 'Quad chart updated succesfully';
        }
        $chartID = $this->xMetricsChartsRepository->saveCharts($details, (isset($details['chart_id'])) ? $details['chart_id'] : '');
        return new JsonResponse(array('msg' => $response_text, 'status' => 'success', 'id' => $chartID));
    }

    public function addChartDetailsAction(Request $request) {
        $chartData = json_decode($request->getContent(), true);
        $chartId = $chartData['chart_id'];
        foreach ($chartData['section_data'] as $section) {
            $sectionData = array();
            $sectionData['section_name'] = $section['name'];
            $sectionData['section_type'] = $section['type'];
            $sectionData['chart_id'] = $chartId;
            $sectionId = $this->xMetricsChartsSectionRepository->saveChartSections($sectionData, $section['id']);
            $this->addKpi($section, $chartId, $sectionId);
        }
        return new JsonResponse(array('msg' => 'x-matrix updated successfully', 'status' => 'success'));
    }

    public function removeKpiRelation($param) {
        $this->xMetricsChartRelationsRepository->removeKpiRelation($param);
        return new JsonResponse(array('msg' => 'Relation removed succesfully', 'status' => 'success'));
    }

    public function addKpiRelationAction(Request $request) {
        $params = json_decode($request->getContent(), true);
        $isRelationExist = $this->xMetricsChartRelationsRepository->checkRelationExist($params);
        if (!isset($isRelationExist[0])) {
            $this->removeKpiRelation($params);
            $this->xMetricsChartRelationsRepository->saveRelations($params);
        } else {
            $this->removeKpiRelation($params);
        }
        return new JsonResponse(array('msg' => 'Relation added succesfully', 'status' => 'success'));
    }

    public function getChartAction(Request $request) {
        $params = json_decode($request->getContent(), true);
        $data = $this->getChartDetails($params['id']);
        return new JsonResponse(array('data' => $data, 'status' => 'success', 'msg' => 'x-matrix updated'));
    }

    private function getChartDetails($id) {
        $result = $this->xMetricsChartsRepository->getDetails($id);
        if (!count($result)) {
            return $result;
        } else {
            $relations = $this->xMetricsChartsRepository->getConnections($id);
            $types = ['top', 'left', 'right', 'bottom', 'user'];
            $data = array();
            $er_flag = '0';
            $data['connections'] = $relations;
            foreach ($result as $res) {
                $data[$res['section_type']]['id'] = $res['section_id'];
                $data[$res['section_type']]['name'] = $res['section_name'];
                $data[$res['section_type']]['type'] = $res['section_type'];
                $kpiData = [];
                $kpiData['id'] = $res['chart_kpi_id'];
                $kpiData['kpi_id'] = $res['kpi_id'];
                $kpiData['label'] = $res['kpi_name'];
                $data[$res['section_type']]['data'][] = $kpiData;
            }

            foreach ($types as $type) {
                if (!isset($data[$type])) {
                    $er_flag = 1;
                    $sectionDetails = $this->xMetricsChartsRepository->getSectionDetails($id, $type);
                    $data[$type]['id'] = isset($sectionDetails[0]['id']) ? $sectionDetails[0]['id'] : '';
                    $data[$type]['name'] = isset($sectionDetails[0]['name']) ? $sectionDetails[0]['name'] : '';
                    $data[$type]['type'] = $type;
                    $kpiData = [];
                    $kpiData['id'] = '';
                    $kpiData['kpi_id'] = '';
                    $kpiData['label'] = '';
                    $data[$type]['data'][] = $kpiData;
                }
            }
            if ($er_flag == 1) {
                $data['verify']['is_valid'] = 'invalid';
            } else {
                $data['verify']['is_valid'] = 'valid';
            }
            return $data;
        }
    }

    private function addKpi($section, $chartId, $sectionId) {
        if (!empty($section['data'])) {
            foreach ($section['data'] as $kpiData) {
                $this->xMetricsChartsKpiRepository->saveKpi(array('kpi_name' => $kpiData['kpi_label'], 'chart_id' => $chartId, 'section_id' => $sectionId, 'kpi_id' => (int) $kpiData['kpi_id'], 'sort_order' => '0', 'is_deleted' => $kpiData['is_deleted']), $kpiData['id']);
            }
        }
    }

    private function testData() {
        return '{"chart_id":1,
            "section_data":[{
                "name": "top name",
                "type":"top",
                "data": [
                    {
                        "kpi_id":"",
                        "kpi_label": "element T07"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element T08"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element T09"
                    }
                ]
            },
            {
                "name": "left side name",
                "type":"left",
                "data": [
                    {
                        "kpi_id":"",
                        "kpi_label": "element L01"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element L02"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element L03"
                    }
                ]
            },
            {
                "name": "right side name",
                "type":"right",
                "data": [
                    {
                        "kpi_id":"",
                        "kpi_label": "element R04"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element R05"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element R06"
                    }
                ]
            },
           {
                "name": "users",
                "type":"user",
                "data": [
                    {
                        "kpi_id":"",
                        "kpi_label": "element U13"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element U14"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element U15"
                    }
                ]
            },
           {
                "name": "bottom name",
                "type":"bottom",
                "data": [
                    {
                        "kpi_id":"",
                        "kpi_label": "element B10"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element B11"
                    },
                    {
                        "kpi_id":"",
                        "kpi_label": "element B12"
                    }
                ]
            }
        ]}';
    }

}
