<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TableController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('table/x-metrix.html.twig', [
            'controller_name' => 'TableController',
        ]);
    }
}
