<?php

namespace App\Repository;

use App\Entity\XMetricsChartRelations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method XMetricsChartRelations|null find($id, $lockMode = null, $lockVersion = null)
 * @method XMetricsChartRelations|null findOneBy(array $criteria, array $orderBy = null)
 * @method XMetricsChartRelations[]    findAll()
 * @method XMetricsChartRelations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XMetricsChartRelationsRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, XMetricsChartRelations::class);
    }

    public function saveRelations($params) {

        if (isset($params['rid']) && $params['rid'] != '') {
            $relation = $this->_em->getRepository('App:XMetricsChartRelations')->find($params['rid']);
        } else {
            $relation = new XMetricsChartRelations();
        }
        if (isset($params['chart_id'])) {
            $relation->setChartId($params['chart_id']);
        }
        if (isset($params['row'])) {
            $relation->setRowChartKpi($params['row']);
        }
        if (isset($params['column'])) {
            $relation->setColumnChartKpi($params['column']);
        }
        if (isset($params['type'])) {
            $relation->setType($params['type']);
        }
        if (isset($params['created_by'])) {
            $relation->setCreatedBy($params['created_by']);
        }
        if (isset($params['created_at'])) {
            $relation->setCreatedAt($params['created_at']);
        }
        $this->_em->persist($relation);
        $this->_em->flush();
        $params['relation_id'] = $relation->getId();
        return $response = array(
            'relaition_id' => $params['relation_id']
        );
    }

    public function removeKpiRelation($param) {
        if (isset($param)) {
            $query = $this->createQueryBuilder('rm')
                    ->delete()
                    ->where('rm.chartId=:id', 'rm.rowChartKpi=:row_id', 'rm.columnChartKpi=:column_id')
                    ->setParameter('id', $param['chart_id'])
                    ->setParameter('row_id', $param['row'])
                    ->setParameter('column_id', $param['column']);
            return $query->getQuery()->execute();
        }
    }

    public function checkRelationExist($param) {
        $qb = $this->_em->createQueryBuilder();
        $query = $qb->select('mr.id')
                ->from('App:XMetricsChartRelations', 'mr')
                ->where('mr.chartId=:chart_id', 'mr.rowChartKpi=:row', 'mr.columnChartKpi=:column')
                ->setParameter('chart_id', $param['chart_id'])
                ->setParameter('row', $param['row'])
                ->setParameter('column', $param['column']);
        return $query->getQuery()->getResult();
    }

}
