<?php

namespace App\Repository;

use App\Entity\XMetricsCharts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method XMetricsCharts|null find($id, $lockMode = null, $lockVersion = null)
 * @method XMetricsCharts|null findOneBy(array $criteria, array $orderBy = null)
 * @method XMetricsCharts[]    findAll()
 * @method XMetricsCharts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XMetricsChartsRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, XMetricsCharts::class);
    }

    public function saveCharts($params,$id='') {
        if ($id) {
            $chart = $this->_em->getRepository('App:XMetricsCharts')->find($id);
        } else {
            $chart = new XMetricsCharts();
        }
        if (isset($params['chart_name'])) {
            $chart->setChartName($params['chart_name']);
        }
        if (isset($params['created_by'])) {
            $chart->setCreatedBy($params['created_by']);
        }
        if (isset($params['created_at'])) {
            $chart->setCreatedAt(new \DateTime());
        }
        if (isset($params['updated_by'])&& $id) {
            $chartSection->setUpdatedBy($params['updated_by']);
        }
        if (isset($params['updated_by_at']) && $id) {
            $chartSection->setUpdatedAt(new \DateTime());
        }
        if (isset($params['is_deleted'])) {
            $chart->setIsDeleted($params['is_deleted']);
        } else {
            $chart->setIsDeleted('0');
        }
        $this->_em->persist($chart);
        $this->_em->flush();
        return $chart->getId();
    }
    
    public function getDetails($id) {
        $qb = $this->_em->createQueryBuilder();
        $query = $qb->select('mc.id as chart_id,mc.chartName as chart_name,ms.id as section_id,ms.name as section_name,ms.sectionType as section_type,kpi.id as chart_kpi_id,kpi.name as kpi_name,kpi.kpiId as kpi_id,kpi.kpiType as kpi_type')
                ->from('App:XMetricsCharts', 'mc')
                ->innerJoin('App:XMetricsChartsSection', 'ms', 'WITH', 'ms.chartId = mc.id')
                ->innerJoin('App:XMetricsChartsKpi', 'kpi', 'WITH', 'kpi.sectionId = ms.id')
                ->where('mc.isDeleted=0', 'ms.isDeleted=0', 'kpi.isDeleted=0','mc.id= :id')
                ->groupBy('ms.id,kpi.id')
                ->setParameter('id', $id);
        return $query->getQuery()->getResult();
    }
    
    public function getSectionDetails($id,$sectionType) {
         $qb = $this->_em->createQueryBuilder();
         $query = $qb->select('ms.id,ms.name')
                ->from('App:XMetricsChartsSection', 'ms')
                ->where('ms.chartId=:chart_id', 'ms.isDeleted=0','ms.sectionType=:sectionType')
                ->setParameter('chart_id', $id)
                 ->setParameter('sectionType', $sectionType);
        return $query->getQuery()->getResult();
    }
    public function getConnections($id) {
        $qb = $this->_em->createQueryBuilder();
         $query = $qb->select('mr.rowChartKpi as row,mr.columnChartKpi as column,mr.type')
                ->from('App:XMetricsChartRelations', 'mr')
                ->where('mr.chartId=:chart_id')
                ->setParameter('chart_id', $id);
        return $query->getQuery()->getResult();
    }

}
