<?php

namespace App\Repository;

use App\Entity\XMetricsChartsKpi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method XMetricsChartsKpi|null find($id, $lockMode = null, $lockVersion = null)
 * @method XMetricsChartsKpi|null findOneBy(array $criteria, array $orderBy = null)
 * @method XMetricsChartsKpi[]    findAll()
 * @method XMetricsChartsKpi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XMetricsChartsKpiRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, XMetricsChartsKpi::class);
    }

    public function saveKpi($params,$id='') {
  
        
        if ($id) { // ckpid = x_metrics_charts_kpi.id
            $chartKpi = $this->_em->getRepository('App:XMetricsChartsKpi')->find($id);
        } else {
            $chartKpi = new XMetricsChartsKpi();
        }
        if (isset($params['chart_id'])) {
            $chartKpi->setChartId($params['chart_id']);
        }
        if (isset($params['kpi_id'])) { //kpi_id = x_metrics_charts_kpi.kpi_id
            $chartKpi->setKpiId($params['kpi_id']);
        }
        if (isset($params['kpi_type'])) {
            $chartKpi->setKpiType($params['kpi_type']);
        }
        if (isset($params['sort_order'])) {
            $chartKpi->setSortOrder($params['sort_order']);
        }
        if (isset($params['section_id'])) {
            $chartKpi->setSectionId($params['section_id']);
        }
        if (isset($params['kpi_name'])) {
            $chartKpi->setName($params['kpi_name']);
        }
        if (isset($params['section_type'])) {
            $chartKpi->setSectionType($params['section_type']);
        }
        if (isset($params['created_by'])) {
            $chartKpi->setCreatedBy($params['created_by']);
        }
        if (isset($params['created_at']) && $id) {
            $chartKpi->setCreatedAt(new \DateTime());
        }
        if (isset($params['updated_by'])) {
            $chartKpi->setUpdatedBy($params['updated_by']);
        }
        if (isset($params['updated_by_at']) && $id) {
            $chartKpi->setUpdatedAt(new \DateTime());
        }
        if (isset($params['is_deleted'])) {
            $chartKpi->setIsDeleted($params['is_deleted']);
        } else {
            $chartKpi->setIsDeleted('0');
        }
        $this->_em->persist($chartKpi);
        $this->_em->flush();
        return $chartKpi->getId();
    }

}
