<?php

namespace App\Repository;

use App\Entity\XMetricsChartsSection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method XMetricsChartsSection|null find($id, $lockMode = null, $lockVersion = null)
 * @method XMetricsChartsSection|null findOneBy(array $criteria, array $orderBy = null)
 * @method XMetricsChartsSection[]    findAll()
 * @method XMetricsChartsSection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XMetricsChartsSectionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, XMetricsChartsSection::class);
    }

    public function saveChartSections($params,$id='') {
        if ($id) {
            $chartSection = $this->_em->getRepository('App:XMetricsChartsSection')->find($id);
        } else {
            $chartSection = new XMetricsChartsSection();
        }
        if (isset($params['chart_id'])) {
            $chartSection->setChartId($params['chart_id']);
        }
        if (isset($params['section_name'])) {
            $chartSection->setName($params['section_name']);
        }
        if (isset($params['section_type'])) {
            $chartSection->setSectionType($params['section_type']);
        }
        if (isset($params['created_by'])) {
            $chartSection->setCreatedBy($params['created_by']);
        }
        if (isset($params['created_at'])) {
            $chartSection->setCreatedAt(new \DateTime());
        }
        if (isset($params['updated_by']) && $id) {
            $chartSection->setUpdatedBy($params['updated_by']);
        }
        if (isset($params['updated_by_at']) && $id) {
            $chartSection->setUpdatedAt(new \DateTime());
        }
        if (isset($params['is_deleted'])) {
            $chartSection->setIsDeleted($params['is_deleted']);
        } else {
            $chartSection->setIsDeleted('0');
        }
        $this->_em->persist($chartSection);
        $this->_em->flush();
        return $chartSection->getId();
    }

}
